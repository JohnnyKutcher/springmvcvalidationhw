package org.springmvcvalidation.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springmvcvalidation.dao.StudentDao;
import org.springmvcvalidation.model.Student;

import javax.transaction.Transactional;
import java.util.List;

@Service("studentService")
@Transactional
public class StudentServiceImpl implements StudentService {


    @Autowired
    private StudentDao studentDao;

    @Override
    public Student findById(int id) {
        return studentDao.findById(id);
    }

    @Override
    public void saveStudent(Student student) {
        studentDao.saveStudent(student);
    }

    @Override
    public void updateStudent(Student student) {
        Student entity = studentDao.findById(student.getId());
        if (entity != null) {
            entity.setFirstName(student.getFirstName());
            entity.setLastName(student.getLastName());
            entity.setSex(student.getSex());
            entity.setDob(student.getDob());
            entity.setEmail(student.getEmail());
            entity.setSection(student.getSection());
            entity.setCountry(student.getCountry());
            entity.setFirstAttempt(student.isFirstAttempt());
            entity.setSubject(student.getSubject());
        }
    }

    @Override
    public void deleteStudentByEmail(String email) {
        studentDao.deleteStudentByEmail(email);
    }

    @Override
    public List<Student> findAllStudents() {
        return studentDao.findAllStudents();
    }

    @Override
    public Student findStudentByEmail(String email) {
        return studentDao.findStudentByEmail(email);
    }

    @Override
    public boolean isStudentEmailUnique(Integer id, String email) {
        Student student = findStudentByEmail(email);
        return ( student == null || ((id != null) && (student.getId() == id))) ;
    }

}
