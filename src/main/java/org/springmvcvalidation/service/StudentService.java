package org.springmvcvalidation.service;

import org.springmvcvalidation.model.Student;

import java.util.List;

public interface StudentService {

    Student findById(int id);

    void saveStudent(Student student);

    void updateStudent(Student student);

    void deleteStudentByEmail(String email);

    List<Student> findAllStudents();

    Student findStudentByEmail(String email);

    boolean isStudentEmailUnique(Integer id, String email);

}
