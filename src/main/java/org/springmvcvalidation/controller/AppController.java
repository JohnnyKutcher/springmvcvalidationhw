package org.springmvcvalidation.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springmvcvalidation.model.Student;
import org.springmvcvalidation.service.StudentService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Controller
@RequestMapping("/")
public class AppController {

    @Autowired
    StudentService service;

    @Autowired
    MessageSource messageSource;

    @RequestMapping(value = {"/", "/list"}, method = RequestMethod.GET)
    public String listOfStudents(ModelMap model) {

        List<Student> students = service.findAllStudents();
        model.addAttribute("students", students);
        return "allstudents";
    }

    @RequestMapping(value = {"/new"}, method = RequestMethod.GET)
    public String newRegistration(ModelMap model) {
        Student student = new Student();
        model.addAttribute("student", student);
        model.addAttribute("edit", false);
        return "enroll";
    }

    @RequestMapping(value = { "/new" }, method = RequestMethod.POST)
    public String saveRegistration(@Valid Student student, BindingResult result, ModelMap model) {

        if (result.hasErrors()) {
            return "enroll";
        }

        if (!service.isStudentEmailUnique(student.getId(), student.getEmail())) {
            FieldError emailError = new FieldError("student", "email",
                    messageSource.getMessage("non.unique.email", new String[]{student.getEmail()}, Locale.getDefault()));
            result.addError(emailError);
            return "enroll";
        }

        service.saveStudent(student);

        model.addAttribute("success", "Dear " + student.getFirstName()
                + ", your Registration completed successfully");

        return "success";
    }

    @RequestMapping(value = { "/edit-{email}-student"}, method = RequestMethod.GET)
    public String editStudent(@PathVariable String email, ModelMap model) {
        Student student = service.findStudentByEmail(email);
        model.addAttribute("student", student);
        model.addAttribute("edit", true);
        return "enroll";
    }

    @RequestMapping(value = "/edit-{email}-student", method = RequestMethod.POST)
    public String updateStudent(@Valid Student student, BindingResult result, ModelMap model, @PathVariable String email) {
        if (result.hasErrors()) {
            return "enroll";
        }

        if (!service.isStudentEmailUnique(student.getId(), student.getEmail())) {
            FieldError error = new FieldError("student", "email",
                    messageSource.getMessage("non.unique.email", new String[]{student.getEmail()}, Locale.getDefault()));
            result.addError(error);
            return "enroll";
        }

        service.updateStudent(student);
        model.addAttribute("success", "Dear " + student.getFirstName()
        + ", your profile updated successfully");
        return "success";
    }

    @RequestMapping(value = { "/delete-{ssn}-student"}, method = RequestMethod.GET)
    public String deleteStudent(@PathVariable String email) {
        service.deleteStudentByEmail(email);
        return "redirect:/list";
    }

    @ModelAttribute("sections")
    public List<String> initializeSections() {
        List<String> sections = new ArrayList<>();
        sections.add("Graduate");
        sections.add("Post Graduate");
        sections.add("Research");
        return sections;

    }

    @ModelAttribute("countries")
    public List<String> initializeCountries(){
        List<String> countries = new ArrayList<>();
        countries.add("USA");
        countries.add("CANADA");
        countries.add("FRANCE");
        countries.add("GERMANY");
        countries.add("ITALY");
        countries.add("OTHER");
        return countries;
     }

     @ModelAttribute("subjects")
     public List<String> initializeSubjects(){
         List<String> subjects = new ArrayList<>();
         subjects.add("Physics");
         subjects.add("Chemistry");
         subjects.add("Life Science");
         subjects.add("Political Science");
         subjects.add("Computer Science");
         subjects.add("Mathematics");
         return subjects;
     }

}
