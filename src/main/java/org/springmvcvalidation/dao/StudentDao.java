package org.springmvcvalidation.dao;

import org.springmvcvalidation.model.Student;

import java.util.List;

public interface StudentDao {

    Student findById(int id);

    void saveStudent(Student student);

    void deleteStudentByEmail(String email);

    List<Student> findAllStudents();

    Student findStudentByEmail(String email);

}
