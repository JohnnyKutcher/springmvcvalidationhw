package org.springmvcvalidation.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springmvcvalidation.model.Student;

import java.util.List;

@Repository("studentDao")
public class StudentDaoImpl extends AbstractDao<Integer, Student> implements StudentDao {

    @Override
    public Student findById(int id) {
        return getByKey(id);
    }

    @Override
    public void saveStudent(Student student) {
        persist(student);
    }

    @Override
    public void deleteStudentByEmail(String email) {
        Query query = getSession().createSQLQuery("delete from Student where email =: email");
        query.setString("email", email);
        query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Student> findAllStudents() {
        Criteria criteria = createEntityCriteria();
        return (List<Student>) criteria.list();
    }

    @Override
    public Student findStudentByEmail(String email) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("email", email));
        return (Student) criteria.uniqueResult();
    }
}
