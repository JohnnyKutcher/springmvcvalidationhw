package org.springmvcvalidation.model;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "STUDENT")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Size(min = 3, max = 30)
    @Column(name = "FIRSTNAME", nullable = false)
    private String firstName;

    @Size(min = 3, max = 30)
    @Column(name = "LASTNAME", nullable = false)
    private String lastName;

    @NotEmpty
    @Column(name = "SEX", nullable = false)
    private String sex;

    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Past
    @NotNull
    @Column(name = "DATE_OF_BIRTH", nullable = false)
    private Date dob;

    @Email
    @NotEmpty
    @Column(name = "EMAIL", unique = true, nullable = false)
    private String email;

    @NotEmpty
    @Column(name = "SECTION", nullable = false)
    private String section;

    @NotEmpty
    @Column(name = "COUNTRY", nullable = false)
    private String country;

    @Column(name = "FIRSTATTEMPT", nullable = false)
    private boolean firstAttempt;

    @NotEmpty
    @Column(name = "SUBJECT", nullable = false)
    private String subject;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isFirstAttempt() {
        return firstAttempt;
    }

    public void setFirstAttempt(boolean firstAttempt) {
        this.firstAttempt = firstAttempt;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Student))
            return false;
        Student other = (Student) obj;
        if (id != other.id)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Student{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", sex='" + sex + '\'' +
                ", dob=" + dob +
                ", email='" + email + '\'' +
                ", section='" + section + '\'' +
                ", country='" + country + '\'' +
                ", firstAttempt=" + firstAttempt +
                ", subject=" + subject +
                '}';
    }
}
