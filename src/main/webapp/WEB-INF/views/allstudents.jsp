<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>University Enrollment</title>

    <style>

        div{
            position: fixed;
            background-color: #E8E1E1;
            border:  1px solid #ddd;
            border-radius: 4px;
        }
        tr:first-child{
            font-weight: bold;
            background-color: #C6C9C4;
        }
    </style>

</head>


<body>
<div>
<h2>List of Students</h2>
<table>
    <tr>
        <td>First Name</td><td>Last Name</td><td>Sex</td><td>Date of birth</td><td>Email</td>
        <td>Section</td><td>Country</td><td>Subject</td>
    </tr>
    <c:forEach items="${students}" var="student">
        <tr>
            <td>${student.firstName}</td>
            <td>${student.lastName}</td>
            <td>${student.sex}</td>
            <td>${student.dob}</td>
            <td>${student.section}</td>
            <td>${student.country}</td>
            <td>${student.subject}</td>
            <td><a href="<c:url value='/edit-${student.email}-student' />">${student.email}</a></td>
            <td><a href="<c:url value='/delete-${student.email}-employee' />">delete</a></td>
        </tr>
    </c:forEach>
</table>
<br/>
<a href="<c:url value='/new' />">Add New Student</a>
</div>
</body>
</html>